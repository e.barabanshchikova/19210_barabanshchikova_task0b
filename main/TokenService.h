//#pragma once

#include <iostream>
#include <vector>

using namespace std;

class TokenService {
private:
    bool ifSymbol(char symb);
public:
    vector<string> splitLine(string str);
};