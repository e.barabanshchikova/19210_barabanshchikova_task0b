//#pragma once

#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

class WordStatService {
private:
    map <string, int> dictionary;
    int countWord;
    vector< pair<string, pair<int, double>> > table;

    void getCountWord();
    float getFrequency(int const num);
    void setTable();

public:
    WordStatService();
    void insertWords(vector<string> words);
    vector<pair<string, pair<int, double>>>  getTable();
};