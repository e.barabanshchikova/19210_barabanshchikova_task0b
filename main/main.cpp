﻿#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include "FileRecoder.h"
#include "TokenService.h"
#include "WordStatService.h"
#include "ReportService.h"

int main(int arg, char* argv[]) {
    if (arg != 3) {
        cout << "incorrect number of arguments";
        return 0;
    }

    FileRecoder FileRec(argv[1]);
    if (!FileRec.isOpen())
        cout << "Cannot open the file.\n";
    else {
        TokenService wordList;

        WordStatService dict;

        while (FileRec.nextLine()) {
            dict.insertWords(wordList.splitLine(FileRec.gettingLine()));
        }

        ReportService FileOut(argv[2]);
        if (!FileOut.isOpen())
            cout << "Cannot open the file.\n";
        else
            FileOut.printTab(dict.getTable());
    }

    return 0;
}
