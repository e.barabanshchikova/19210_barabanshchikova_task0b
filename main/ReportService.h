#pragma once
#include <iostream>
#include<vector>
#include<fstream>
using namespace std;

class ReportService {
private:
    string path;
    ofstream file;
    bool status;
public:
    ReportService(string newPath);
    void printTab(vector< pair<string, pair<int, double>>> table);
    bool isOpen();
    ~ReportService();
};