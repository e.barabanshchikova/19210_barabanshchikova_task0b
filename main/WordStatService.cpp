#include "WordStatService.h"
#include <iostream>

using namespace std;

void WordStatService::getCountWord() {
    countWord = 0;
    map <string, int> ::iterator it = dictionary.begin();
    for (int i = 0; it != dictionary.end(); i++, it++)
        countWord += it->second;
}

float WordStatService::getFrequency(int const num) { return ((float)num) / ((float)countWord) * 100.0; }

void WordStatService::setTable() {
    table.clear();
    map <string, int> ::iterator it = dictionary.begin();

    for (int i = 0; it != dictionary.end(); i++, it++) {
        table.push_back(make_pair(it->first, make_pair(it->second, getFrequency(it->second))));
    }

    sort(table.begin(), table.end(), [](auto& left, auto& right) -> bool {
        return left.second.first > right.second.first;
        });
}

WordStatService::WordStatService() { countWord = 0; }

void WordStatService::insertWords(vector<string> words) {
    map <string, int> ::iterator it = dictionary.begin();
    for (int i = 0; i < (int)words.size(); i++) {
        it = dictionary.find(words[i]);
        if (dictionary.end() == it)
            dictionary.insert(pair<string, int>(words[i], 1));
        else
            dictionary.insert(pair<string, int>(words[i], ++it->second));
    }
}

vector<pair<string, pair<int, double>>>  WordStatService::getTable() {
    getCountWord();
    setTable();
    return table;
}