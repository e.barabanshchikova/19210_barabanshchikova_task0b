#include "FileRecoder.h"
#include <iostream>

using namespace std;

FileRecoder::FileRecoder(string newPath) {
    line = "";
    path = "\0";
    path = newPath;
    file.open(path);
    if (!file.is_open())
        status = false; //cannot open the file
    else
        status = true; //the file open
}

bool FileRecoder::nextLine() {
    line = "\0";
    if (!getline(file, line).eof())
        return true;
    else
        return false;
}

string FileRecoder::gettingLine() { return line; }

bool FileRecoder::isOpen() { return status; }

FileRecoder::~FileRecoder() { file.close(); }