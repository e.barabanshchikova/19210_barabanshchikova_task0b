#include "pch.h"
#include "../main/FileRecoder.h"
#include "../main/TokenService.h"
#include "../main/WordStatService.h"
#include "../main/ReportService.h"
#include <iostream>

FileRecoder::FileRecoder(string newPath) {
    line = "";
    path = "\0";
    path = newPath;
    file.open(path);
    if (!file.is_open())
        status = false; //cannot open the file
    else
        status = true; //the file open
}

bool FileRecoder::nextLine() {
    line = "\0";
    if (!getline(file, line).eof())
        return true;
    else
        return false;
}

string FileRecoder::gettingLine() { return line; }

bool FileRecoder::isOpen() { return status; }

FileRecoder::~FileRecoder() { file.close(); }

bool TokenService::ifSymbol(char symb) {
    if ((symb >= 'a' && symb <= 'z') || (symb >= '0' && symb <= '9'))
        return true;
    else
        return false;
}

vector<string> TokenService::splitLine(string str) {
    vector<string> wordList;
    string word = "\0";
    for (int i = 0; i <= (int)str.size(); i++) {
        char symbol = (char)tolower((char)str[i]);  //������ ������ � ������� ��������
        if (ifSymbol(symbol)) //���� ������ ��� ����� ��� ����� �� ��������� ��� � �����
            word += symbol;
        else {
            if (word[0] != '\0') {
                word += "\0";
                wordList.push_back(word);
            }
            word = "\0";
        }
    }
    return wordList;
}

void WordStatService::getCountWord() {
    countWord = 0;
    map <string, int> ::iterator it = dictionary.begin();
    for (int i = 0; it != dictionary.end(); i++, it++)
        countWord += it->second;
}

float WordStatService::getFrequency(int const num) { return ((float)num) / ((float)countWord) * 100.0; }

void WordStatService::setTable() {
    table.clear();
    map <string, int> ::iterator it = dictionary.begin();

    for (int i = 0; it != dictionary.end(); i++, it++) {
        table.push_back(make_pair(it->first, make_pair(it->second, getFrequency(it->second))));
    }

    sort(table.begin(), table.end(), [](auto& left, auto& right) -> bool {
        return left.second.first > right.second.first;
        });
}

WordStatService::WordStatService() { countWord = 0; }

void WordStatService::insertWords(vector<string> words) {
    map <string, int> ::iterator it = dictionary.begin();
    for (int i = 0; i < (int)words.size(); i++) {
        it = dictionary.find(words[i]);
        if (dictionary.end() == it)
            dictionary.insert(pair<string, int>(words[i], 1));
        else
            dictionary.insert(pair<string, int>(words[i], ++it->second));
    }
}

vector<pair<string, pair<int, double>>>  WordStatService::getTable() {
    getCountWord();
    setTable();
    return table;
}

ReportService::ReportService(string newPath) {
    path = newPath;
    file.open(path);
    if (!file.is_open())
        status = false; //cannot open the file
    else
        status = true; //the file open
}

void ReportService::printTab(vector< pair<string, pair<int, double>>> table) {
    int size = table.size();
    for (int i = 0; i < size; i++) {
        file << table[i].first << ";" << table[i].second.first << ";" << table[i].second.second << '\n';
    }
}

bool ReportService::isOpen() { return status; }
ReportService::~ReportService() { file.close(); }

TEST(FileRecoderTests, ConstrTests) {
    FileRecoder test1("C:\\Users\\Helen\\Desktop\\programms\\2nd course\\OOP\\lab0b\\restest.txt");
    ASSERT_TRUE(test1.isOpen() == 1);
    FileRecoder test2("");
    ASSERT_TRUE(test2.isOpen() == 0);
}

TEST(TokenServiceTests, splitLineTests) {
	TokenService test;
	ASSERT_TRUE(test.splitLine("//..,.%%^^())--=+////").empty());
}

TEST(WordStatServiceTests, InsertWordsTests) {
    vector<string> testVect;
    testVect.push_back("one");
    testVect.push_back("two");
    testVect.push_back("three");
    testVect.push_back("four");
    testVect.push_back("five");
    testVect.push_back("six");
    WordStatService testDict;
    testDict.insertWords(testVect); testVect.clear();

    ASSERT_TRUE(testDict.getTable().size() == 6);
    testVect.push_back("one");
    testVect.push_back("one");
    testVect.push_back("one");
    testVect.push_back("one");
    testVect.push_back("xyz");
    testVect.push_back("zxy");
    testDict.insertWords(testVect); testVect.clear();
    ASSERT_TRUE(testDict.getTable().size() == 8);
    ASSERT_TRUE(testDict.getTable()[0].second.second == float((float(5.0)) / (float(12.0)) * 100.0));
    ASSERT_TRUE(testDict.getTable()[0].second.first == 5);
}
